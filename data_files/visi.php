<html>
    <head>
    <title>Visi ieraksti</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../st.css">
    </head>   
    <body>
    <h2>PHP.MYSQL.FORMS</h2>
    
    <?php
    include("../conf.php");
    $con = new mysqli($host, $user, $psw, $db);
    
    if($con -> connect_error) {
    	die("Kļūda: ".$con -> connect_error);
    } 
    	echo"<h3> Visi ieraksti no tabulas 'eks'</h3>";
    	echo"<p> Savienojums ar DB ir izveidots </p>";
    	$con -> query("SET NAMES 'utf-8'");
    	
    	$query = "SELECT * FROM `eks` WHERE 1";
    	$result = mysqli_query($con, $query) or die("Kļūda: ".$con -> connect_error($con));
    	$rows = mysqli_num_rows($result);
    	$fields = mysqli_num_fields($result);
    	echo "<table border = '1'><tr><th>ID</th> <th>Vārds</th> <th>Uzvārds</th> <th>Dzim.datums</th> <th>Prof.Novirze</th> <th>Kurs</th> <th>Stipendija</th> <th>E-pasts</th></tr>";
    	
    	for($i = 0; $i < $rows; $i++) {
    		$row = mysqli_fetch_row($result);
    		echo"<tr>";
    		for($j = 0; $j < $fields; $j++) {
    			echo"<td>$row[$j]</td>";
    		}
    		echo"</tr>";
    	}
    	echo"</table>";
    
    ?>
    </body>
</html>