<html>
    <head>
    <title>Dzēst datus</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../st.css">
    </head>   
    <body>
    <h2>PHP.MYSQL.FORMS</h2>
    <h3>Datu dzēšana no DB</h3>
    
    <?php
    include("../conf.php");
    $con = new mysqli($host, $user, $psw, $db);
    
    if($con -> connect_error) {
    	die("Kļūda: ".$con -> connect_error);
    } 
    	echo"<p> Savienojums ar DB ir izveidots </p>";
    	$con -> query("SET NAMES 'utf-8'");
    	
    	echo"<p>Rezultāts pirms dzēšanas:</p>";
    	
    	$con -> query("SET NAMES 'utf-8'");
    	$query = "SELECT `id`, `vards`, `uzvards`,`dzim` FROM `eks` WHERE `kurs` = 'absolvents'";
		$result = mysqli_query($con, $query) or die("Kļūda: ".$con -> connect_error($con));
		$rows = mysqli_num_rows($result);
    	echo "<table border=1> <tr><th>ID</th><th>Vārds</th><th>Uzvārds</th><th>Kurss</th></tr>";
			for($i = 0; $i<$rows; $i++) {
				$row = mysqli_fetch_row($result);
				echo "<tr>";
					for($j = 0; $j< 4; $j++) {
						echo "<td>$row[$j]</td>";}
				echo "</tr>";}
		echo "</table>";		
		
		echo"<p>Dzēšam datus.</p>";
		
    	$query = "DELETE FROM `eks` WHERE `kurs` = 'absolvents'";
    	$result = mysqli_query($con, $query) or die("Kļūda ".mysqli_error($con));
    	
    	echo"<p>Rezultāts pēc dzēšanas:</p>";
    	
    	$con -> query("SET NAMES 'utf-8'");
    	$query = "SELECT `id`, `vards`, `uzvards`,`dzim` FROM `eks` WHERE `kurs` = 'absolvents'";
		$result = mysqli_query($con, $query) or die("Kļūda: ".$con -> connect_error($con));
		$rows = mysqli_num_rows($result);
    	echo "<table border=1> <tr><th>ID</th><th>Vārds</th><th>Uzvārds</th><th>Kurss</th></tr>";
			for($i = 0; $i<$rows; $i++) {
				$row = mysqli_fetch_row($result);
				echo "<tr>";
					for($j = 0; $j< 4; $j++) {
						echo "<td>$row[$j]</td>";}
				echo "</tr>";}
		echo "</table>";		
		
    	echo"<p>Sarakstā absolventu nav</p>";
    	?>
    	
    	</body>
   </html>