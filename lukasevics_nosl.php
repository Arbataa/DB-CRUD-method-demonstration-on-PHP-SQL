<html>
	<head>
		<title>Eksāmens</title>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="st.css">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		</head>
	<body>
		<h1 align="center">PHP. MYSQL. (Lukaševičs Armands Jānis, 3.p)</h1>
		<table border="1" align="center">
			<tr valign="top">
				<td width="400">
				<h3>Datu ievads.</h3>
	    		<form action="data_files/datu_ievads.php" method="post" target="blank">
			    <h4>Vārds: <input type="text" name="vards"></h4>
			    <h4>Uzvārds: <input type="text" name="uzvards"></h4>
			    <h4>Dzimšanas datums: <input type="date" name="dzim"></h4>
			    <h4>Profesionāla novirze: 
			    <select name="prof"> 
			    <option value="datori">Datori</option>
			    <option value="programmēšana">Programmēšana</option>
			    <option value="multimedija">Multimēdija</option>
			    <option value="loģistika">Loģistika</option>
			    <option value="grāmatvedība">Grāmatvedība</option>
			    </select> </h4>
			    <h4><input type="radio" name="kurs" value="1.k"> 1.Kurss
			    <input type="radio" name="kurs" value="2.k"> 2.Kurss
			    <input type="radio" name="kurs" value="3.k"> 3.Kurss
			    <input type="radio" name="kurs" value="4.k"> 4.Kurss</h4>
			    <h4>Stipendija:<input tyoe="number" name="stip"></h4>
			    <h4>E-pasts: <input type="text" name="epasts">(@jak125.lv) </h4>
			    <h4><input type="submit" value="Ievadiet datus"></h4>
		    </form>
		    
		    <hr>
		    
		    <form action="data_files/visi.php" method="post" target="blank">
		    	<h4 align="center">Visi tabulas eks ieraksti</h4>
		    	<h4 align="center"><input type="submit" value="VISI DATI"></h4>
		    </form>
				</td>
				<td width="400">
					<h3>Datu atlase</h3>
					<form action="data_files/prof_atlase.php" method="post" target="blank">
					<h4>Pēc specialitātes</h4>
						<h4>
			    		<select multiple name="atlase[]" size=5>
						    <option value="datori">Datori</option>
						    <option value="programmēšana">Programmēšana</option>
						    <option value="multimedija">Multimēdija</option>
						    <option value="loģistika">Loģistika</option>
						    <option value="grāmatvedība">Grāmatvedība</option>
			    		</select>
		    		</h4>
	    			<h4><input type="submit" value="Izvēlēties specialitāti"></h4>
	    </form>
	    
	    <hr>
	    
	    <form action="data_files/gad_atlase.php" method="post" target="blank">
	    	<h4>Pēc gada un mēneša (fragments)</h4>
	    	<h4>Ievadiet gadu:<input type="number" name="gads"></h4>
	    	<h4>Mēneša nosaukuma fragments (en):<input type="text" name="men"></h4>
			<h4><input type="submit" value="Parādīt rezultātus"></h4>
	    </form>
				</td>
				<td width="400">
					<h3>Datu maiņa</h3>
					
					<h4>Minimālās stipendijas definēšana</h4>
					<p>Ievadiet minimālās stipendijas izmēru:</p>
					<form action="data_files/stip.php" method="post" target="blank">
						<p><input type="number" name="stip"></p>
					<h4><input type="submit" value="Apstirpināt izmaiņas"></h4>
					</form>
					
					<hr>
					
					<h4>Pārcelšana uz nākamo kursu</h4>
					<form action="data_files/parcelt.php" method="post" target="blank">
					<h4><input type="submit" value="PĀRCELT STUDENTUS"></h4>
					</form>
					
					<hr>
					
					<h4>Arhīva tabulas izveide</h4>
					<form action="data_files/arhivs.php" method="post" target="blank">
					<h4><input type="submit" value="Veidot tabulu 'arhivs'"></h4>
					</form>	
					
					<hr>
					<h4>Arhīva tabulas papildināšana</h4>
					<form action="data_files/papildArhiv.php" method="post" target="blank">
						<h4><input type="submit" value="Papildināt tabulu'arhivs'"></h4>
					</form>	
					
					<hr>
					<h4>Datu dzēšana</h4>
					<form action="data_files/dzestAbs.php" method="post" target="blank">
						<h4><input type="submit" value="DZĒST NO DB ABSOLVENTUS"></h4>
					</form>	
													
					
				</td>
			</tr>		
		</table>
	</body>
	
</html>