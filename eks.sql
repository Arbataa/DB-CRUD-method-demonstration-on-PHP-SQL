-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 30, 2022 at 04:17 PM
-- Server version: 8.0.31-0ubuntu0.20.04.1
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `4p33`
--

-- --------------------------------------------------------

--
-- Table structure for table `eks`
--

CREATE TABLE `eks` (
  `id` int NOT NULL,
  `vards` varchar(30) NOT NULL,
  `uzvards` varchar(30) NOT NULL,
  `dzim` date NOT NULL DEFAULT '2005-01-01',
  `prof` set('programmēšana','loģistika','multimedija','datori','grāmatvedība') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `kurs` set('1.k','2.k','3.k','4.k','absolvents') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `stip` decimal(5,2) NOT NULL,
  `epasts` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `eks`
--

INSERT INTO `eks` (`id`, `vards`, `uzvards`, `dzim`, `prof`, `kurs`, `stip`, `epasts`) VALUES
(1, 'Jānis', 'Bērziņš', '2005-01-01', 'programmēšana', '2.k', '100.00', 'janis.berzins@jak125.lv'),
(2, 'Arnis', 'Antonovs', '2007-02-02', 'loģistika', '3.k', '75.00', 'arnis.antonovs@jak125.lv'),
(3, 'Viktors', 'Semjenovs', '2005-11-21', 'programmēšana', '1.k', '29.00', ' 	viktors.semjenovs@jak125.lv'),
(4, 'Igors', 'Zvirbulis', '2005-05-03', 'loģistika', '1.k', '137.00', 'igors.zvirbulis@jak125.lv'),
(5, 'Andris', 'Liepiņš', '2006-11-11', 'programmēšana', '2.k', '85.00', 'andris.liepins@jak125.lv'),
(6, 'Andris', 'Ūpitis', '2005-01-30', 'datori', '3.k', '90.00', 'andris.upitis@jak125.lv'),
(7, 'Aleksandrs', 'Kalniņš', '2004-10-12', 'datori', '4.k', '87.00', 'aleksandrs.kalnins@jak125.lv'),
(8, 'Jānis', 'Ozoliņš', '2005-11-03', 'multimedija', '3.k', '51.00', 'janis.ozolins@jak125.lv'),
(9, 'Andris', 'Okuņevs', '2005-06-21', 'programmēšana', '2.k', '41.00', 'andris.okunevs@jak125.lv'),
(10, 'Jurijs', 'Eglitis', '2005-09-02', 'loģistika', '2.k', '97.00', 'juris.eglitis@jak125.lv'),
(11, 'Antons', 'Ignatjevs', '2004-11-21', 'programmēšana', '4.k', '65.00', 'antons.ignatjevs@jak125.lv'),
(12, 'Aleks', 'Irbitis', '2004-05-21', 'multimedija', '4.k', '54.00', 'aleks.irbitis@jak125.lv'),
(13, 'Jevgenijs', 'Aleksandrovs', '2005-02-03', 'multimedija', '3.k', '100.00', 'jevgenijs.aleksandrovs@jak125.lv'),
(14, 'Līga', 'Antoneviča', '2005-01-01', 'grāmatvedība', '3.k', '121.00', 'liga.antonevica@jak125.lv'),
(15, 'Aleksejs', 'Ignatjevs', '2007-02-02', 'loģistika', '2.k', '104.00', 'aleksejs.ignatjevs@jak125.lv'),
(16, 'Andris', 'Ločmelis', '2005-01-01', 'loģistika', '3.k', '149.00', 'andris.locmelis@jak125.lv'),
(17, 'Anna', 'Smolova', '2007-10-10', 'datori', '2.k', '47.00', 'anna.smolova@jak125.lv'),
(18, 'Jānis', 'Krumiņš', '2008-01-01', 'datori', '1.k', '55.00', 'janis.krumins@jak125.lv'),
(19, 'Anna', 'Indriksone', '2004-02-12', 'grāmatvedība', '4.k', '65.00', 'anna.indriksone@jak125.lv'),
(20, 'Agris', 'Vilcāns', '2008-10-12', 'programmēšana', '2.k', '57.00', 'agris.vilcans@jak125.lv'),
(21, 'Anita', 'Vecāre', '2010-10-12', 'grāmatvedība', '1.k', '120.00', 'anita.vecare@jak125.lv'),
(22, 'Inga', 'Vecāre', '2007-01-01', 'multimedija', '2.k', '69.00', 'inga.vecare@jak125.lv'),
(23, 'Ingus', 'Ozoliņš', '2006-11-11', 'multimedija', '3.k', '84.00', 'ingus.ozolins@jak125.lv'),
(24, 'Inga', 'Vecāre', '2007-01-01', 'multimedija', '2.k', '69.00', 'inga.vecare@jak125.lv'),
(25, 'Ingus', 'Ozoliņš', '2006-11-11', 'multimedija', '3.k', '84.00', 'ingus.ozolins@jak125.lv'),
(26, 'Sintija', 'Ekermane', '2008-01-01', 'grāmatvedība', '1.k', '89.00', 'sintija.ekermane@jak125.lv'),
(27, 'Sergejs', 'Viktorovs', '2006-10-09', 'programmēšana', '3.k', '101.00', 'sergejs.viktorovs@@jak125.lv'),
(28, 'Aleksejs', 'Zorins', '2008-05-12', 'grāmatvedība', '1.k', '101.00', 'aleksejs.zorins@jak125.lv');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `eks`
--
ALTER TABLE `eks`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `eks`
--
ALTER TABLE `eks`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
